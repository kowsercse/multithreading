/*----------------------------------------------------------------------------

  Name:

    matmul_mt.c

  Purpose:

    To compute matrix multiplication (multithreaded version)

  Description:

    Computationally intensive applications benifit from the use of all
    available processors. Matrix multiplication is a good example.

    The main function acquires the optional number of CPUs to be used
    for matrix multiplication. If the number of CPUs is not supplied
    from the command line, it assumes all CPUs available on the system
    by calling the sysconf function. Then it calls the matmul function
    to perform matrix multiply.

    When the matrix multiply function (matmul) is called, it acquires
    a mutex (mul_lock) to ensure that only one matrix multiply is in
    progress. This relies on mutexes that are statically initialised
    to zero (zeroed memory allocation). Actually, all mutexes and
    condition variables in this example are statically initialised to
    zero. The requesting thread (matmul) then acquires another mutex
    lock (work.lock) and checks whether its worker threads have been
    created. If not, it creates one for each CPU. Once the worker
    threads have been created, it sets up the work structure including
    the counters of work to do and not done.  Then it signals the
    worker threads via a conditional variable (work.start_cond)
    broadcasting. Then it waits until receiving the work done signal
    (work_done_cond) from one of workers, and then releases the mutex
    locks (work.lock and mul_lock) such that the next matrix multiply
    can proceed.

    Each worker waits until receiving the signal from the requesting
    thread using the mutex work.lock and the condition variable
    work.start_cond. It picks off a row and column from the input
    matrices and updates the counter of work to do (work.todo--), so
    that the next worker will get the next item. It then release the
    mutex work.lock so that the vector product can proceed in
    parallel. When the results are ready, the worker acquires the
    mutex lock and updates the counter of work completed
    (work.notdone). The worker which completes the last bit of work
    signals the requesting thread (work.lock and work.done_cond).

  Usage:

    Compile the program by typing:

      gcc -o matmul_mt matmul_mt.c -pthread

    and run it by typing:

      matmul_mt [no_of_CPU]

      (for example, matmul_mt [4])

-----------------------------------------------------------------------------*/

/* Include Files */
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

/* Constant */
#define SZ 50

/* Data Type Declarations */
typedef struct {
  int     (*m1)[SZ][SZ], (*m2)[SZ][SZ], (*m3)[SZ][SZ];
  int     row, col;
  int     todo, notdone, workers;
  pthread_mutex_t lock;
  pthread_cond_t  start_cond, done_cond;
} Work_struct;

/* Shared Data Declarations */
Work_struct work;
pthread_t    thread[20]; // Thread array.maximum number of threads 20.
pthread_mutex_t     mul_lock;
int         nCPU;               /* number of CPUs to be used */

/*
 * worker - Pick up a row and column from the input matrices
 *          and calculate a vector product for one entry in
 *          the resulting matrix
 */
void *worker(void *arg)
{
  int (*m1)[SZ][SZ], (*m2)[SZ][SZ], (*m3)[SZ][SZ];
  int row, col, i, result;

  while(1) {

    /*
     * Acquire a mutex lock (work.lock) to work with
     * the conditional variable (work.start_cond)
     */

    pthread_mutex_lock (&work.lock);

    /*
     * If nothing to do (work.todo == 0), wait on the conditional variable
     * (work.start_cond) until somthing needs to be done,
     * which will be signaled from the requesting thread
     */
    while (work.todo == 0)
      pthread_cond_wait (&work.start_cond,&work.lock);

    /*
     * Each worker picks off a row and column from the input matrices
     * and updates the counter of work so that the next worker will get
     * the next item
     */
    work.todo--;

    m1 = work.m1; m2 = work.m2; m3 = work.m3;
    row = work.row; col = work.col;
    work.col++;

    if (work.col == SZ) {
      work.col = 0;
      work.row++;
      if (work.row == SZ)
        work.row = 0;
    }

    /*
     * Release the mutex lock (work.lock) so that the vector
     * product can proceed in parallel
     */

    pthread_mutex_unlock (&work.lock);

    /* Vector product resulting in one entry in the result matrix */
    result = 0;
    for (i = 0; i < SZ; i++)
      result += (*m1) [row][i] * (*m2) [i][col];
    (*m3) [row][col] = result;

    /*
     * Reacquire the mutex (work.lock) and update the counter of work
     * completed. The worker that completes the last bit of work
     * signals the requesting thread
     */

    pthread_mutex_lock (&work.lock);
    work.notdone--;

    if (work.notdone == 0)
      pthread_cond_signal(&work.done_cond);
    pthread_mutex_unlock (&work.lock);

  } /* while (1) */
}

/*
 * matmul - Multithreaded version of matrix multiply, m3 = m1 * m2
 */
void matmul(int (*m1)[SZ][SZ], int (*m2)[SZ][SZ], int (*m3)[SZ][SZ])
{
  int i;

  /*
   * Acquire a mutex lock (mul_lock) to ensure that only
   * one matrix multiply is in progress
   */

  pthread_mutex_lock (&mul_lock);

  /*
   * Acquire a mutex lock (work.lock) to work together with
   * the conditional variable (work.start_cond)
   */

  pthread_mutex_lock (&work.lock);

  /*
   * Check if its worker threads have been created.
   * If not, create one for each CPU.
   */
  if (work.workers == 0) {
    work.workers = nCPU;

    for (i = 0; i < work.workers; i++)
      if(pthread_create(&thread [i], NULL, worker, NULL)){
        exit(1);
      }
  }

  /*
   * Initialise the work structure for matrix multiply
   * Set up counters of work to do and not done
   */
  work.m1 = m1; work.m2 = m2; work.m3 = m3;
  work.row = work.col = 0;
  work.todo = work.notdone = SZ * SZ;

  /*
   * Signal the workers via the condition variable (work.start_cond)
   * broadcasting
   */

  pthread_cond_broadcast(&work.start_cond);

  /*
   * Check on the work done condition (work.notdone == 0), waits
   * until the last bit of work is completed (signalled from one
   * of worker via the conditional variable work.done_cond)
   * and release the work lock so that each worker can strat to work
   */

   while (work.notdone != 0)
     pthread_cond_wait (&work.done_cond,&work.lock);

  /* Release the work lock associated with the conditional variable */

  pthread_mutex_unlock (&work.lock);

  /* Release the mutex lock (mul_lock)
   * such that the next matrix multiply can proceed
   */

  pthread_mutex_unlock (&mul_lock);

}

int main( int argc, char *argv[] ) {

  int *m1, *m2, *m3;
  int i, j;

  /*
   * Acquire the optional number of CPUs from the command line
   * If not supplied, get the number of CPUs available on the system
   */
  if ( argc == 1 )
    nCPU = sysconf (_SC_NPROCESSORS_ONLN);
  else if ( argc == 2 )
    nCPU  = atoi( argv[1] );
  else {
    printf("Usgae: matmul_mt [no_of_CPU]\n");
    printf("e.g.,  matmul_mt [4]\n");
  }

  printf("Use %d CPUs for multipling two matrices of size %dx%d\n",nCPU,SZ,SZ);

  m1 = (int *)malloc(SZ*SZ*sizeof(int));
  m2 = (int *)malloc(SZ*SZ*sizeof(int));
  m3 = (int *)malloc(SZ*SZ*sizeof(int));

  for (i = 0; i < SZ; i++)
	  for (j = 0; j < SZ; j++){
		  *(m1+i*SZ+j) = 4 + i + j;
		  *(m2+i*SZ+j) = 5 + i + j;
	  }


 /* call matmul function to compute m3 = m1 * m2 */
  matmul((int (*)[SZ][SZ])m1, (int (*)[SZ][SZ])m2, (int (*)[SZ][SZ])m3);

/*

  printf("\nPrint Product Matrix m3: \n");
  for (i = 0; i < SZ; i++) {
    for (j = 0; j < SZ; j++)
       printf("%d\t", m1[i*SZ+j]);
    printf("\n");
  }

  printf("\nPrint Product Matrix m3: \n");
  for (i = 0; i < SZ; i++) {
    for (j = 0; j < SZ; j++)
       printf("%d\t", m2[i*SZ+j]);
    printf("\n");
  }

  printf("\nPrint Product Matrix m3: \n");
  for (i = 0; i < SZ; i++) {
    for (j = 0; j < SZ; j++)
       printf("%d\t", m3[i*SZ+j]);
    printf("\n");
  }
*/

  return(0);
}








